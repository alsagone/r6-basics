import json
import functools

# Sort operators by release date


def sort_operators(a, b):
    returnValue = 0

    if (a["Year"] > b["Year"]):
        returnValue = -1

    elif (a["Year"] < b["Year"]):
        returnValue = 1

    elif (a["Season"] > b["Season"]):
        returnValue = -1

    elif (a["Season"] < b["Season"]):
        returnValue = 1

    else:
        returnValue = 1 if (a["Side"] == "A") else -1

    return returnValue


if __name__ == "__main__":
    attackers = []
    defenders = []

    with open("r6-operators.json") as f:
        data = json.load(f)

    for operator in data:
        side = operator["Side"]

        if ("A" in side):
            attackers.append(operator)

        if ("D" in side):
            defenders.append(operator)

    attackers.sort(key=functools.cmp_to_key(lambda a, b: sort_operators(a, b)))
    defenders.sort(key=functools.cmp_to_key(lambda a, b: sort_operators(a, b)))

    with open("attackers.json", "w+") as a:
        json.dump(attackers, a, indent=4)

    with open("defenders.json", "w+") as d:
        json.dump(defenders, d, indent=4)
