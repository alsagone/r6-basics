import Head from "next/head";
import Image from "next/image";
import Link from "next/link";

import Ace from "../public/ace.png";
import Maestro from "../public/maestro.webp";
import Osa from "../public/osa.webp";

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>R6: the Basics</title>
        <meta
          name="description"
          content="Learn the basics of Rainbow Six Siege"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="background" />

      <h1 className="title uppercase">Rainbow Six Siege: the Basics</h1>
      <h3 className="title">
        Rainbow Six Siege taught by a beginner, for beginners
      </h3>
      <div className="categories">
        <Link href="/prerequisites" passHref>
          <div className="category">
            <Image src={Osa} alt="Prerequisites" />
            <span className="category-title uppercase">Prerequisites</span>
          </div>
        </Link>
        <Link href="/attack" passHref>
          <div className="category">
            <Image src={Ace} alt="Attack" />
            <span className="category-title uppercase">Attack</span>
          </div>
        </Link>

        <Link href="/defense" passHref>
          <div className="category">
            <Image src={Maestro} alt="Defense" />
            <span className="category-title uppercase">Defense</span>
          </div>
        </Link>
      </div>

      <div className="footer">
        <span>
          Website by{" "}
          <a
            href="https://twitter.com/alsagone"
            rel="norelopener noreferrer"
            target="_blank"
          >
            alsagone
          </a>
        </span>
      </div>
    </div>
  );
}
