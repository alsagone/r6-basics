import Words from "../public/vocabulary.json";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";

function renderTerm(word, definition, image) {
  return (
    <Card
      classname="term"
      sx={{ maxWidth: 345 }}
      style={{
        backgroundColor: "rgba(1,1,1,0.3)",
        margin: "10px 10px",
      }}
    >
      <CardMedia component="img" image={image} alt={word} />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div" color="#fff">
          {word}
        </Typography>
        <Typography variant="body2" color="#fff">
          {definition}
        </Typography>
      </CardContent>
    </Card>
  );
}
export default function Vocabulary() {
  return (
    <div className="vocabulary">
      {Words.map((word) => {
        return renderTerm(word.Term, word.Definition, word.Picture);
      })}
    </div>
  );
}
