import "../styles/globals.css";
import "../styles/style.css";
import "../styles/home.css";
import "../styles/prerequisites.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
