import Head from "next/head";
import styles from "../styles/style.css";

export default function Home() {
  return (
    <div>
      <Head>
        <title>R6: the Basics</title>
        <meta
          name="description"
          content="Learn the basics of Rainbow Six Siege"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

          <div className={styles.background} />
      <div className={styles.container}>
        <h1 className={styles.title}>Rainbow Six Siege: the Basics</h1>
      </div>
    </div>
  );
}
