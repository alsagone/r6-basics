import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import AttackOps from "../public/attack_operators.webp";

export default function Attack() {
  return (
    <div className="container">
      <Head>
        <title>R6: the Basics - Attack</title>
        <meta
          name="description"
          content="Learn the basics of Rainbow Six Siege"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="background" />

      <h1 className="title uppercase">Rainbow Six Siege: the Basics</h1>
      <h3 className="title">Attack</h3>
      <section>
        <h2 className="section-title">Attacking operators</h2>
        <p>
          I started to make a list of every attacking operator but I realized it
          would be hard to keep it up to date overtime and hard to read on
          mobile devices.
          <br />
          Instead, I coded an other{" "}
          <Link
            href="https://alsagone.gitlab.io/r6-quick-guide"
            alt="R6 Quick Guide"
            target="_blank"
            rel="norelopener noreferrer"
          >
            website
          </Link>{" "}
          where you can easily get a short description of every operator&quot;s
          ability and/or gadget in the game.
        </p>
        <Image src={AttackOps} alt="Attackers" width="800" height="450" />
        <br />
        I&quot;ll say it again: it&quot;s completely normal to feel totally lost
        when you start learning how to play a new operator.
        <br />
        The more you&quot;ll play, the more you&quot;ll know how to use your
        operator&quot;s ability/gadget more efficiently.
      </section>
      <section>
        <h2 className="section-title">How to attack?</h2>
        <ul>
          <li>The preparation phase</li>
          <p>
            At the start of every round, you spawn &#34;in&#34; your drone and
            you have 45 seconds to find the objective. <br />
            On your way, you can scan for enemies and their icon will be
            displayed on top of your screen (don&quot;t worry, I know there are
            a lot of operators but I assure you: you&quot;ll quickly remember
            their name and what they do)
          </p>
        </ul>
      </section>
    </div>
  );
}
