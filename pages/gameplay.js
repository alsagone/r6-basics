import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Link from "next/link";

export default function Gameplay() {
  return (
    <section>
      <h2 className="section-title">Gameplay</h2>
      <p>
        A team of 5 attackers faces a team of 5 defenders. The attackers try to
        take control of the objective and the defenders have to prevent them
        from doing that. There are 3 different game modes:
      </p>
      <div className="flex-row">
        <Card
          sx={{ maxWidth: 345 }}
          style={{
            backgroundColor: "rgba(1,1,1,0.3)",
            margin: "10px 10px",
          }}
        >
          <CardMedia component="img" image="Screenshots/bomb.png" alt="Bomb" />
          <CardContent>
            <Typography variant="body2" color="#fff">
              <ul>
                <li>Attackers must locate and disard one of the two bombs.</li>
                <li>
                  Defenders have to prevent the defuser from being planted or
                  deactivate it.
                </li>
              </ul>
            </Typography>
          </CardContent>
        </Card>

        <Card
          sx={{ maxWidth: 345 }}
          style={{
            backgroundColor: "rgba(1,1,1,0.3)",
            margin: "10px 10px",
          }}
        >
          <CardMedia
            component="img"
            image="Screenshots/secure_area.png"
            alt="Secure Area"
          />
          <CardContent>
            <Typography variant="body2" color="#fff">
              <ul>
                <li>Teams fight to control a defined room of the map.</li>
                <li>
                  Attackers have to occupy the room for 10 consecutive seconds
                  or kill all the Defenders.
                </li>
                <li>
                  The capture progress is halted if a Defender is in the room.
                </li>
              </ul>
            </Typography>
          </CardContent>
        </Card>

        <Card
          sx={{ maxWidth: 345 }}
          style={{
            backgroundColor: "rgba(1,1,1,0.3)",
            margin: "10px 10px",
          }}
        >
          <CardMedia
            component="img"
            image="Screenshots/hostage.png"
            alt="Secure Area"
          />
          <CardContent>
            <Typography variant="body2" color="#fff">
              <ul>
                <li>
                  Defenders hold a person hostage and Attackers have to retrieve
                  it and bring it to the extraction point.
                </li>
                <li>
                  The team responsible for killing the hostage (if that occurs)
                  loses the round.
                </li>
              </ul>
            </Typography>
          </CardContent>
        </Card>
      </div>
      <p>
        Each round starts with a 45-seconds-long{" "}
        <span className="italic bold">preparation phase</span> where attackers
        have to find the objective with their drones and defenders have to set
        up the objective by reinforcing it and placing their gadgets.
      </p>
      <p>
        We&quot;ll dive more into how a round plays out from both sides in the
        dedicated <Link href="/Attack">Attack</Link> and{" "}
        <Link href="/Defend">Defense</Link> sections.
      </p>
    </section>
  );
}
