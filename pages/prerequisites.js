import Head from "next/head";
import Image from "next/image";
import Placeholder from "../public/placeholder.png";
import Profile from "../public/PP_Pride.png";

import Gameplay from "./gameplay";
import Vocabulary from "./vocabulary";

export default function Prerequisites() {
  return (
    <div className="container">
      <Head>
        <title>R6: the Basics</title>
        <meta
          name="description"
          content="Learn the basics of Rainbow Six Siege"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="background" />

      <h1 className="title uppercase">Rainbow Six Siege: the Basics</h1>
      <h3 className="title">Prerequisites</h3>

      <div className="intro">
        <div className="intro-container">
          <section>
            <h2 className="section-title">Introduction</h2>
            <Image
              className="profile"
              src={Profile}
              alt="Me"
              width="150px"
              height="150px"
            />
            <p>
              Hello ! Before starting allow me to introduce myself, I&apos;m
              <a
                href="https://twitter.com/alsagone"
                rel="norelopener noreferrer"
                target="_blank"
              >
                alsagone
              </a>
              . I play Siege for a while now and I introduced my best friend to
              the game a month ago. It made me realize how difficult the
              onboarding in this game is: even if there is a tutorial, it only
              gives you surface knowledge and you rapidly feel overwhelmed in
              your first multiplayer matches.
              <p>
                Moreover, the tutorial doesn&apos;t really prepare you to the
                &quot;real world&quot; because there are so much gameplay
                mechanics the tutorial doesn&apos;t tell you about and from the
                point of view the person who&apos;s supposed to teach the game
                to my friend, there were moments where I remembered that I
                completely forgot to tell them about some things which made me
                feel like a bad teacher.
              </p>
              <p>
                So, I decided to use my coding skills to build this website
                that&apos;ll aim at giving you all the knowledge you need to hop
                on Siege :)
              </p>
            </p>{" "}
            <p>
              As you can see by my{" "}
              <a
                href="https://r6stats.com/stats/81818e63-adb7-44ac-a7ce-46735518ccf8/"
                rel="norelopener noreferrer"
                target="_blank"
              >
                stats
              </a>
              , I&apos;m just an average player so I won&apos;t be able to give
              you advanced tips like which attachements to use on your weapons
              etc... but as I said, the basic knowledge is enough to start !
            </p>
            <p>
              PS: the learning curve is rough, you will die A LOT at the
              beginning but don&apos;t worry, everybody had this phase and
              don&apos;t get demoralized: it&apos;s absolutely normal !
            </p>
          </section>
          <section>
            <h2 className="section-title">Where to start ?</h2>
            <p>
              Congratulations, you have successfully purchased and installed one
              of the best First Person Shooters of this generation !<br />
              To start your learning process: you can start by doing the
              &quot;Situations&quot; (
              <span className="italic">
                Main Menu -&gt; Play -&gt; Learning Area
              </span>
              ) They are designed to get a hand on the game in a safe and
              controlled environnement with no pressure at all, you&quot;ll also
              learn the basic gameplay vocabulary like{" "}
              <span className="italic">breaching, soft/reinforced walls</span>
              etc... Don&quot;t focus on the secondary objectives yet (unless
              you&quot;re a completionnist), you can directly do the next
              situation even if you didn&quot;t complete the previous one at
              100%.
            </p>
            <p>
              Next, you can start practicing your aim in the{" "}
              <span className="italic">Training Grounds</span> but first go in
              the Options Menu (the little cog on the top-right of the main menu
              -&gt; Options), select{" "}
              <span className="italic">Matchmaking Preferences</span>. Turn off{" "}
              <span className="italic">Protect Hostage</span> and{" "}
              <span className="italic">Extract Hostage</span>
              <br />
              In the maps: turn everything off except for{" "}
              <span className="italic">House</span>
              <br />
              Why ? Short answer: Elimination is the shortest game mode out of
              the three (you just have to run and gun) and House is the smallest
              map of the game.
            </p>
          </section>
          <Gameplay />
          <section>
            <h2 className="section-title">Vocabulary</h2>
            <p>
              In this section, we&quot; tackle the difficult list every
              important word/concept you&quot; encounter during your journey
              with Siege
            </p>
            <Vocabulary />
          </section>
        </div>
      </div>
    </div>
  );
}
